Name:          tftp
Version:       5.2
Release:       32
Summary:       The client for the Trivial File Transfer Protocol (TFTP)
License:       BSD
Group:         Applications/Internet
URL:           http://www.kernel.org/pub/software/network/tftp/
Source0:       http://www.kernel.org/pub/software/network/tftp/tftp-hpa/tftp-hpa-%{version}.tar.bz2
Source1:       tftp.socket
Source2:       tftp.service
Patch0000:     tftp-0.40-remap.patch
Patch0002:     tftp-hpa-0.39-tzfix.patch
Patch0003:     tftp-0.42-tftpboot.patch
Patch0004:     tftp-0.49-chk_retcodes.patch
Patch0005:     tftp-hpa-0.49-fortify-strcpy-crash.patch
Patch0006:     tftp-0.49-cmd_arg.patch
Patch0007:     tftp-hpa-0.49-stats.patch
Patch0008:     tftp-hpa-5.2-pktinfo.patch
Patch0009:     tftp-doc.patch
Patch0010:     tftp-enhanced-logging.patch
Patch0011:     tftp-hpa-5.2-gcc10.patch

Patch6001:     backport-Update-manpage-to-match-source-code-for-map-file.patch

BuildRequires: readline-devel autoconf systemd-units
BuildRequires: gcc

%description
TFTP which is the abbreviation of Trivial File Fransfer Protocol
enables users to transfer files between local host and remote machine.
This package tftp is a client implementing TFTP.

%package server
Summary: The server for the Trivial File Transfer Protocol (TFTP)
Requires: systemd-units
Requires(post): systemd-units
Requires(postun): systemd-units

%description server
The TFTP server is run by using systemd socket activation, and is disabled by default.

%package_help

%prep
%autosetup -n %{name}-hpa-%{version} -p1
%build
autoreconf
%configure
%make_build

%install
rm -rf %{buildroot}
install -d %{buildroot}%{_bindir} %{buildroot}%{_mandir}/man{1,8} %{buildroot}%{_sbindir}
install -d %{buildroot}%{_localstatedir}/lib/tftpboot %{buildroot}%{_unitdir}

make INSTALLROOT=%{buildroot} SBINDIR=%{_sbindir} MANDIR=%{_mandir} INSTALL='install -p' install
install -m755 -d -p %{buildroot}%{_sysconfdir}/xinetd.d/ %{buildroot}%{_localstatedir}/lib/tftpboot
sed -e 's:/var:%{_localstatedir}:' -e 's:/usr/sbin:%{_sbindir}:' \
 tftp-xinetd > %{buildroot}%{_sysconfdir}/xinetd.d/tftp
touch -r tftp-xinetd %{buildroot}%{_sysconfdir}/xinetd.d/tftp

install -p -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}
install -p -m 0644 %{SOURCE2} %{buildroot}%{_unitdir}

%post server
%systemd_post tftp.socket

%preun server
%systemd_preun tftp.socket

%postun server
%systemd_postun_with_restart tftp.socket


%files
%doc README README.security CHANGES
%{_bindir}/tftp

%files server
%config(noreplace) %{_sysconfdir}/xinetd.d/tftp
%dir %{_localstatedir}/lib/tftpboot
%{_sbindir}/in.tftpd
%{_unitdir}/*

%files help
%{_mandir}/man1/*
%{_mandir}/man8/*


%changelog
* Fri Aug 02 2024 chengyechun <chengyechun1@huawei.com> - 5.2-32
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update the info of patch

* Tue Oct 18 2022 chengyechun <chengyechun1@huawei.com> - 5.2-31
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Update manpage to match source code for map file

* Fri Jul 30 2021 gaihuiying <gaihuiying1@huawei.com> - 5.2-30
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix build error with gcc 10

* Thu May 27 2021 lijingyuan <lijingyuan3@huawei.com> - 5.2-29
- Type:bugfix 
- ID:NA
- SUG:NA
- DESC:Add the compilation dependency of gcc.

* Tue Dec 15 2020 xihaochen <xihaochen@huawei.com> - 5.2-28
- Type:requirement
- ID:NA
- SUG:NA
- DESC:remove sensitive words 

* Tue Dec 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 5.2-27
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add the systemd-units in requires for server package

* Tue Sep 17 2019 Zaiwang Li<lizaiwang1@huawei.com> - 5.2-26
- Init package
